<?php
/**
 * Created by PhpStorm.
 * User: hchoang
 * Date: 30/1/17
 * Time: 11:32 PM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('url', TextType::class, array(
                'required' => false
            ))
            ->add('price', NumberType::class, array(
                'required' => false
            ))
            ->add('save', SubmitType::class);
    }
}