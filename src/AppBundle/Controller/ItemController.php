<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Item;
use AppBundle\Form\ItemType;
use Buzz\Message\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;

class ItemController extends BaseController
{
    /**
     * @Route("/items", name="item_list")
     */
    public function listAction(Request $request)
    {
        $repository = $this->getRepository('AppBundle:Item');
        $items = $repository->findAll();
        $headers = array(
            'id'=> array(
                'name' => 'ID',
            ),
            'name' => array(
                'name' => 'Name',
            ),
            'price' => array(
                'name' => 'Item Price',
            )
        );
        return $this->render('crud/list.html.twig', array(
            'table' => array(
                'title' => 'Item List',
                'headers' => $headers,
                'data' => $items
            )
        ));
    }


    /**
     * @Route("/items/{id}", name="item_detail")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request)
    {
        return $this->render('crud/view.html.twig');
    }

    /**
     * @Route("/item/new", name="new_item")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $item = new Item();
        $form = $this->createForm(ItemType::class, $item);
        $em = $this->getManager();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();

            $em->persist($item);
            $em->flush();

            return $this->redirectToRoute('item_list');
        }

        return $this->render('crud/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("items/{id}/update_price", name="item_price_update")
     * @param Request $request
     */
    public function updatePriceAction(Request $request, $id)
    {
        $repo = $this->getRepository('AppBundle:Item');
        /** @var Item $item */
        $item = $repo->find($id);

        $browser = $this->get('gremo_buzz');
        /** @var Response $res */
        $res = $browser->get($item->getUrl());
        $content = $res->getContent();
        $crawler = new Crawler($content);
        $product = $crawler->filter('.detail-item-shelfproduct.shelfProductStamp');

    }

    private function updatePrice()
    {

    }
}
