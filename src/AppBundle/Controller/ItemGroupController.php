<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class ItemGroupController extends Controller
{
    /**
     * @Route("/groups")
     */
    public function listAction()
    {
        return $this->render('crud/list.html.twig', array(
            'table' => array(
                'title' => 'Group List'
            )
        ));
    }

    /**
     * @Route("/groups/{id}/update_price")
     * @param Request $request
     */
    public function updatePriceAction(Request $request)
    {

    }

}
