<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

abstract class BaseController extends Controller
{
    /**
     * @param $repositoryName
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository($repositoryName)
    {
        return $this->getDoctrine()->getRepository($repositoryName);
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    public function getManager()
    {
        return $this->getDoctrine()->getManager();
    }

}
